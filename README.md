# alexlab-cloud-fn

> Cloudflare Worker definitions for task automation

Utility endpoints and automation helpers implemented as cloud functions for managing
[Alexlab Cloud](https://gitlab.com/alexlab-cloud).
