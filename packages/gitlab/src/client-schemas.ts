import { z } from "zod";
import { camelize } from "../../web-utils/dist";

export const GitlabGroupSchema = z
    .object({
        id: z.number(),
        description: z.string(),
        full_name: z.string(),
        full_path: z.string(),
        avatar_url: z.string(),
    })
    .transform(camelize);

export const GitlabProjectSchema = z
    .object({
        id: z.number(),
        description: z.string(),
        name: z.string(),
        name_with_namespace: z.string(),
        path: z.string(),
        path_with_namespace: z.string(),
        ssh_url_to_repo: z.string(),
        web_url: z.string(),
        readme_url: z.string(),
        avatar_url: z.string(),
    })
    .transform(camelize);
