import { vendorRequest } from "./utils";
import { z } from "zod";
import { GitlabGroupSchema, GitlabProjectSchema } from "./client-schemas";

import type {
    ApiConfig,
    GitlabGroupData,
    GitlabOrg,
    GitlabProjectData,
} from "./types";

/**
 *
 * @param apiConfig
 * @param groupId
 * @returns
 */
export async function getGroup(
    apiConfig: ApiConfig,
    groupId: number
): Promise<GitlabGroupData> {
    const response = await vendorRequest(apiConfig, `groups/${groupId}`);
    return GitlabGroupSchema.parse(response) as GitlabGroupData;
}

/**
 *
 * @param apiConfig
 * @param groupId
 * @returns
 */
export async function getDescendantGroups(
    apiConfig: ApiConfig,
    groupId: number
): Promise<GitlabGroupData[]> {
    const response = await vendorRequest(
        apiConfig,
        `groups/${groupId}/descendant_groups?pagination=keyset&per_page=20`
    );
    return z.array(GitlabGroupSchema).parse(response) as GitlabGroupData[];
}

/**
 * Get all the projects that belong to a group
 *
 * @param apiConfig
 * @param groupId
 * @returns
 */
export async function getGroupProjects(
    apiConfig: ApiConfig,
    groupId: number
): Promise<GitlabProjectData[]> {
    console.log("running the group project func");
    const response = await vendorRequest(
        apiConfig,
        `groups/${groupId}/projects`
    );
    return z.array(GitlabProjectSchema).parse(response) as GitlabProjectData[];
}

/**
 * Get a single project's information
 *
 * @param apiConfig
 * @param projectId
 * @returns
 */
export async function getProject(
    apiConfig: ApiConfig,
    projectId: number
): Promise<GitlabProjectData> {
    const response = await vendorRequest(apiConfig, `projects/${projectId}`);
    return GitlabProjectSchema.parse(response) as GitlabProjectData;
}
