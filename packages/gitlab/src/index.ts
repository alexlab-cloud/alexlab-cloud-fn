import { error, json, Router, withParams, IRequest } from "itty-router";
import {
    getDescendantGroups,
    getGroup,
    getGroupProjects,
    getProject,
} from "./fn";
import { serviceInfo, resolveDirTree } from "@alexlab-cloud/fn.web-utils";

import { apiConfig } from "./config";
import packageMetadata from "../package.json";

export interface WorkerEnv {
    ROOT_GROUP_ID: number;
}

const router = Router();

router
    // Add some middleware upstream on all routes
    .all("*", withParams)

    // Index: return information about the service
    .get("/", (req: IRequest, env: WorkerEnv, ctx: ExecutionContext) => {
        return Response.json(serviceInfo(packageMetadata));
    })

    // GET single group information
    .get(
        "/groups/:id",
        (req: IRequest, env: WorkerEnv, ctx: ExecutionContext) => {
            return getGroup(apiConfig, parseInt(req.params.id));
        }
    )

    .get(
        "/groups/:id/projects",
        (req: IRequest, env: WorkerEnv, ctx: ExecutionContext) => {
            return getGroupProjects(apiConfig, parseInt(req.params.id));
        }
    )

    // GET an entire organization of groups that descend from the group with ID = `id`
    .get(
        "/groups/:id/org",
        (req: IRequest, env: WorkerEnv, ctx: ExecutionContext) => {
            console.log(req.query);
            if (req.query.flat) {
                console.log("Hitting the right place!");
                return getDescendantGroups(apiConfig, parseInt(req.params.id));
            }

            return;
            return getDescendantGroups(apiConfig, parseInt(req.params.id));
        }
    )

    // GET single project information
    .get(
        "/projects/:id",
        (req: IRequest, env: WorkerEnv, ctx: ExecutionContext) => {
            return getProject(apiConfig, parseInt(req.params.id));
        }
    )

    // 404 for everything else
    .all("*", () => error(404));

export default {
    async fetch(req: Request, env: WorkerEnv, ctx: ExecutionContext) {
        console.log(env.ROOT_GROUP_ID);
        return router.handle(req, env, ctx).then(json).catch(error);
    },
};
