import { z } from "zod";
import { GitlabGroupSchema, GitlabProjectSchema } from "./client-schemas";

export type GitlabGroupData = z.infer<typeof GitlabGroupSchema>;

export type GitlabOrg = Record<string, GitlabGroupData>;

export type GitlabProjectData = z.infer<typeof GitlabProjectSchema>;

export type ApiConfig = {
    baseUrl: URL;
    vendor: string;
};
