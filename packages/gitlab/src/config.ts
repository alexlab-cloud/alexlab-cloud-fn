import { ApiConfig } from "./types";

export const apiConfig: ApiConfig = {
    vendor: "GitLab",
    baseUrl: new URL("https://gitlab.com/api/v4"),
};
