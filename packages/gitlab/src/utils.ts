import path from "node:path";
import { ApiConfig } from "./types";

export type SearchParamComponents = {
    mainParams?: URLSearchParams;
    paginationParams?: URLSearchParams;
};

export type URIComponents = {
    url: URL;
    searchParamComponents: SearchParamComponents;
};

/**
 * Make a request to an external API and return some JSON
 *
 * https://stackoverflow.com/a/58437909/6683107
 *
 * @param url
 * @param queryParams
 * @returns
 */
export async function extRequest<T extends object>(
    url: URL,
    queryParams?: URLSearchParams
): Promise<T | unknown> {
    console.log(typeof queryParams);

    console.log(url.toString() + queryParams);

    try {
        const rawResult = fetch(url.toString());

        const headers = (await rawResult).headers;
        console.log(headers.get("gitlab-sv"));

        const result = fetch(url.toString()).then((response) =>
            response.json()
        ) as T;
        // const test = await result;
        // console.log(test);

        return result;
    } catch (err) {
        throw err;
    }
}

/**
 * Pass an API config and a URI to make a request to a vendor's interface
 *
 * @param apiConfig
 * @param resource
 * @param queryParams
 * @returns
 */
export async function vendorRequest<T extends object>(
    apiConfig: ApiConfig,
    resource: string,
    queryParams?: URLSearchParams
): Promise<T | unknown> {
    try {
        const url = new URL(path.join(apiConfig.baseUrl.toString(), resource));
        return (await extRequest(url, queryParams)) as T;
    } catch (err) {
        throw err;
    }
}

export type Pagination = {
    totalPages: number;
    currentPage: number;
    pageSize: number;
};

export type ApiResult<T> = {
    data: T[];
    headers: Headers;
    pagination: Pagination;
};

export function resolveQueryParams(
    components: SearchParamComponents
): URLSearchParams {
    if (components.mainParams && components.paginationParams) {
        return new URLSearchParams({
            ...components.mainParams,
            ...components.paginationParams,
        });
    }

    return new URLSearchParams();
}

export function extractApiResult<T extends object>(
    response: Response
    paginationHandler: Function: Pagination
): ApiResult<T> {
    return {
        data: [response.json()] as T[],
        headers: response.headers,
        pagination: (paginationHandler: (n: number) => Pagination)
    };
}

export async function fetchApiResults<T extends object>(
    uri: URIComponents
): Promise<ApiResult<T>> {
    try {
        const response = await fetch(
            `${uri.url}?${resolveQueryParams(uri.searchParamComponents)}`
        );
        return (await response) as ApiResult<T>;
    } catch (err) {
        throw err;
    }
}

export async function fetchAllApiResults<T extends object>(
    url: URL,
    perPage: number
) {
    let page = 1;
    let results: T[] = [];
    let totalPages;
}
