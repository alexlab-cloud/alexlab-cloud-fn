import camelcaseKeys from "camelcase-keys";
import { string } from "zod";

export const camelize = <T extends Record<string, unknown>>(obj: T): T =>
    camelcaseKeys(obj) as T;

/**
 * Return true if the input object only has an array at its top level, e.g. {[]}
 *
 * @param obj
 * @returns
 */
export const hasTopLevelObjArr = <T extends Record<string, unknown>>(
    obj: T
): boolean => {
    if (typeof obj !== "object" || obj === null) {
        return false;
    }

    const keys = Object.keys(obj);

    if (keys.length !== 1) {
        return false;
    }

    const arrData = obj[keys[0]];

    if (!Array.isArray(arrData)) {
        return false;
    }

    return arrData.every(
        (item: unknown) =>
            typeof item === "object" &&
            item !== null &&
            item &&
            !Array.isArray(item)
    );
};
