import path from "node:path";
import { z } from "zod"

/**
 * Splits the parameters semantically between those used for pagination and the rest of them.
 */
export type SearchParamComponents = {
    mainParams?: URLSearchParams;
    paginationParams?: URLSearchParams;
};

/**
 * Deconstructed URI type meant for isolating pagination parameters in addition to other parts
 * of the query string and the request path.
 */
export type URIComponents = {
    url: URL;
    path: string;
    searchParamComponents: SearchParamComponents;
};

/**
 * Abstraction of query parameter pagination values for API requests.
 */
export type PaginationControl = {
    currentPage: number;
    pageSize?: number;
    totalPages?: number;
};

/**
 * Basic vendored web service configuration.
 *
 * Include optional pagination adapter function to grab pagination values (page, results per page, total pages).
 */
export type VendorApiConfig = {
    vendor: string;
    baseUrl: URL;
    paginationAdapter?(searchParams: URLSearchParams): PaginationControl;
}

/**
 * Destructured API response information.
 */
export type ApiResult<T> = {
    data: T;
    headers: Headers;
    searchParams: SearchParamComponents;
    pagination: PaginationControl;
};

export function parseQueryParams(params: URLSearchParams, paginationAdapter: (params: URLSearchParams) => PaginationControl): SearchParamComponents {

}

export async function parseDataWithSchema<T>(rawData: Record<string, unknown>, schema: z.AnyZodObject): Promise<T> | Promise<T[]> {
    try {
        if
        return schema.parse(rawData) as T;
    } catch (err) {
        throw err;
    }

}

export async function fetchApiResults<T extends object>(
    url: URL
): Promise<ApiResult<T>> {
    try {
        const response = await fetch(
            url
        );
        return (await response) as ApiResult<T>;
    } catch (err) {
        throw err;
    }
}

export async function fetchAllApiResults<T extends object>(
    url: URL,
    perPage: number
) {
    let page = 1;
    let results: T[] = [];
    let totalPages;
}









/**
 * Make a request to an external API and return some JSON
 *
 * https://stackoverflow.com/a/58437909/6683107
 *
 * @param url
 * @param queryParams
 * @returns
 */
export async function extRequest<T extends object>(
    url: URL,
    queryParams?: URLSearchParams
): Promise<T | unknown> {
    console.log(typeof queryParams);

    console.log(url.toString() + queryParams);

    try {
        const rawResult = fetch(url.toString());

        const headers = (await rawResult).headers;
        console.log(headers.get("gitlab-sv"));

        const result = fetch(url.toString()).then((response) =>
            response.json()
        ) as T;
        // const test = await result;
        // console.log(test);

        return result;
    } catch (err) {
        throw err;
    }
}

/**
 * Pass an API config and a URI to make a request to a vendor's interface
 *
 * @param apiConfig
 * @param resource
 * @param queryParams
 * @returns
 */
export async function vendorRequest<T extends object>(
    apiConfig: ApiConfig,
    resource: string,
    queryParams?: URLSearchParams
): Promise<T | unknown> {
    try {
        const url = new URL(path.join(apiConfig.baseUrl.toString(), resource));
        return (await extRequest(url, queryParams)) as T;
    } catch (err) {
        throw err;
    }
}


export function resolveQueryParams(
    components: SearchParamComponents
): URLSearchParams {
    if (components.mainParams && components.paginationParams) {
        return new URLSearchParams({
            ...components.mainParams,
            ...components.paginationParams,
        });
    }

    return new URLSearchParams();
}

export function extractApiResult<T extends object>(
    response: Response
    paginationHandler: Function: Pagination
): ApiResult<T> {
    return {
        data: [response.json()] as T[],
        headers: response.headers,
        pagination: (paginationHandler: (n: number) => Pagination)
    };
}

export async function fetchApiResults<T extends object>(
    uri: URIComponents
): Promise<ApiResult<T>> {
    try {
        const response = await fetch(
            `${uri.url}?${resolveQueryParams(uri.searchParamComponents)}`
        );
        return (await response) as ApiResult<T>;
    } catch (err) {
        throw err;
    }
}

export async function fetchAllApiResults<T extends object>(
    url: URL,
    perPage: number
) {
    let page = 1;
    let results: T[] = [];
    let totalPages;
}
