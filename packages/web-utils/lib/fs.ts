/**
 * Given an array of paths as strings, return a record that contains the paths according to their
 * filesystem hierarchy
 *
 * @param paths
 * @returns
 */
export function resolveDirTree(paths: string[]): Record<string, any> {
    const dirTree: Record<string, any> = {};

    for (const path of paths) {
        const segments = path.split("/");
        let curNode = dirTree;

        for (const segment of segments) {
            if (!curNode[segment]) {
                curNode[segment] = {};
            }
            curNode = curNode[segment];
        }
    }

    return dirTree;
}
