import z from "zod";

export const ServiceMetadataSchema = z.object({
    name: z.string(),
    version: z.string(),
    description: z.string().optional(),
});

/**
 * A subset of the information that can be found in a package.json file
 */
export type ServiceMetadata = z.infer<typeof ServiceMetadataSchema>;

export function serviceInfo(metadata: ServiceMetadata): ServiceMetadata {
    return ServiceMetadataSchema.parse(metadata) as ServiceMetadata;
}
