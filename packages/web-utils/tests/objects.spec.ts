import { assert } from "chai";

const testInput = [-100, 2, -3, 4, 5, 6, 44, 8, 9, 10];
const testShift = 4;
const expected = [44, 8, 9, 10, -100, 2, -3, 4, 5, 6];

describe("should return a", () => {
    it("should return an array rotated right by a specified number of elements", () => {
        const result = rotateArrRight(testInput, testShift);
        assert.deepEqual(expected, result);
    });
});
